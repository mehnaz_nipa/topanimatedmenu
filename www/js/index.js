angular.module('app', ['onsen'])
    .controller('MyController', ['$scope', function ($scope) {

        $scope.image = 'default_content.png';

        $scope.showGraph = function (code) {

            if (code == 0) {

                $scope.image = 'content.png';

            } else if (code == 1) {

                $scope.image = 'default_content.png';
            }


        };


        // for swipe function
        /*this.photobanner = false;

        document.addEventListener("swipedown", function (e) {
            console.log('Swipe down is detected.');

            return this.photobanner = true;
        });

        document.addEventListener("swipeup", function (e) {
            console.log('Swipe up is detected.');

            return this.photobanner = false;
        });*/


        $scope.openAppBrowser = function (url) {

            cordova.InAppBrowser.open(url, '_blank', 'location = yes');

        }

    }])

    .directive('ngSlideDown', [
        '$timeout',
        function ($timeout) {

            var getTemplate, link;
            getTemplate = function () {
                return '<div><div ng-transclude></div></div>';
            };
            link = function (scope, element, attrs) {
                var closePromise;
                var duration;
                var elementScope;
                var emitOnClose;
                var getHeight;
                var hide;
                var keepAlive;
                var loaded;
                var onClose;
                var openPromise;
                var show;
                var timingFunction;
                
                duration = attrs.duration || 1;
                timingFunction = attrs.timingFunction || 'ease-in-out';
                elementScope = element.scope();
                emitOnClose = attrs.emitOnClose;
                onClose = attrs.onClose;
                closePromise = null;
                openPromise = null;
                keepAlive = attrs.keepAlive !== void 0;
                loaded = false;
                getHeight = function (passedScope) {
                    var c, children, height, _i, _len;
                    height = 0;
                    children = element.children();
                    console.log(children);
                    for (_i = 0, _len = children.length; _i < _len; _i++) {
                        c = children[_i];
                        height += c.clientHeight;
                    }
                    return '' + height + 'px';
                };
                show = function () {
                    if (closePromise) {
                        $timeout.cancel(closePromise);
                    }
                   
                    scope.loaded = true;
                    return $timeout(function () {
                        if (openPromise) {
                            $timeout.cancel(openPromise);
                        }
                        element.find('img').css({
                            overflowY: 'hidden',
                            height: '0px',
                            transition: 'none'
                        }).css({
                            overflowY: 'hidden',
                            transitionProperty: 'height',
                            transitionDuration: '' + duration + 's',
                            transitionTimingFunction: timingFunction,
                            height: '50px'
                        });

                        element.css({
                            overflowY: 'hidden',
                            transitionProperty: 'height',
                            transitionDuration: '' + duration + 's',
                            transitionTimingFunction: timingFunction,
                            height: '108px'
                        });
                        return openPromise = $timeout(function () {
                            return element.css({

                            });
                        }, duration * 900);
                    });
                };
                hide = function () {
                    if (openPromise) {
                        $timeout.cancel(openPromise);
                    }
                    element.css({
                        overflowY: 'hidden',
                        transitionProperty: 'height',
                        transitionDuration: '' + duration + 's',
                        transitionTimingFunction: timingFunction,
                        height: '0px'
                    });

                    element.find('img').css({
                        overflowY: 'hidden',
                        transitionProperty: 'height',
                        transitionDuration: '' + duration + 's',
                        transitionTimingFunction: timingFunction,
                        height: '0px'
                    });
                    if (emitOnClose || onClose) {
                        return closePromise = $timeout(function () {
                            element.find('img').css({
                                height: '0px',
                                transition: 'none'
                            });
                        }, duration * 900);
                    }
                };
                return scope.$watch('expanded', function (value) {
                    if (value) {
                        return $timeout(show);
                    } else {
                        if (value != null) {
                            element.css({height: getHeight()});
                            element[0].clientHeight;
                        }
                        return $timeout(hide);
                    }
                });
            };
            return {
                restrict: 'A',
                scope: {expanded: '=ngSlideDown'},
                transclude: true,
                link: link,
                template: function (tElement, tAttrs) {
                    return getTemplate(tElement, tAttrs);
                }
            };
        }
    ]);


